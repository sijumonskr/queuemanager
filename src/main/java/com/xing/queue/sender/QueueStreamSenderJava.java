package com.xing.queue.sender;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import com.xing.queue.util.QueueConstants;
import org.apache.log4j.Logger;

import com.hdfc.core.core.util.CorePropertyUtil;
import com.xing.queue.util.QueueUtil;
import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPoolToken;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.constants.MQConstants;

/**
 * Class to invoke MQ using Java, the expected input is a BO name, which is
 * compatible with property file values. The class will connect to MQ and will
 * put the message to queue corresponding to the BO.
 */
public class QueueStreamSenderJava implements QueueStreamSender {
	private static final Logger LOG = Logger.getLogger(QueueStreamSenderJava.class);

	private volatile static QueueStreamSenderJava queueStreamSenderJava;

	public static QueueStreamSenderJava getInstance() {
		if (queueStreamSenderJava == null) {
			synchronized (QueueStreamSenderJava.class) {
				if (queueStreamSenderJava == null) {
					queueStreamSenderJava = new QueueStreamSenderJava();
				}
			}
		}
		return queueStreamSenderJava;
	}

	public void invokeQueue(String boName,String message) {
		MQPoolToken token = MQEnvironment.addConnectionPoolToken();
		try {
			Properties connectionProp = CorePropertyUtil.loadClassPathProperties("connection.properties");
			MQQueueManager qMgr = createQueueManager(boName, connectionProp);
			MQQueue queue = createQueue(boName, connectionProp, qMgr);
			addMessageToQueue(queue,message);
			// TODO check and add code if needed to wait for any response after
			// adding
			queue.close();
			qMgr.disconnect();
		} catch (MQException ex) {
			LOG.error("A WebSphere MQ Error occured : Completion Code " + ex.completionCode + " Reason Code "
					+ ex.reasonCode);
			LOG.error(ex.getStackTrace());
			for (Throwable t = ex.getCause(); t != null; t = t.getCause()) {
				LOG.error("... Caused by ");
				LOG.error(t.getStackTrace());
			}
		} catch (java.io.IOException ex) {
			LOG.error("An IOException occured whilst writing to the message buffer: " + ex);
		}
		MQEnvironment.removeConnectionPoolToken(token);
	}

	private void addMessageToQueue(MQQueue queue,String message) throws IOException, MQException {
		MQMessage msg = createQueueMessage(message);
		MQPutMessageOptions pmo = new MQPutMessageOptions();
		queue.put(msg, pmo);
	}

	private MQMessage createQueueMessage(String message) throws IOException {
		MQMessage msg = new MQMessage();
		// TODO check and write appropriate message to the queue
		msg.writeUTF(message);
		return msg;
	}

	private MQQueue createQueue(String boName, Properties connectionProp, MQQueueManager qMgr) throws MQException {
		String queueNameKey = QueueUtil.createQueueNameRequestKey(boName);
		String qName = connectionProp.getProperty(queueNameKey);
		int openOptions = MQConstants.MQOO_INPUT_AS_Q_DEF | MQConstants.MQOO_OUTPUT;
		MQQueue queue = qMgr.accessQueue(qName, openOptions);
		return queue;
	}

	@SuppressWarnings("rawtypes")
	private MQQueueManager createQueueManager(String boName, Properties connectionProp) throws MQException {
		Hashtable queueHash = loadQueueHash(connectionProp);
		String qManager = connectionProp.getProperty(connectionProp.getProperty(QueueConstants.QUEUE_MANAGER_NAME));
		MQQueueManager qMgr = new MQQueueManager(qManager);
		qMgr = new MQQueueManager(qManager, queueHash);
		return qMgr;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Hashtable loadQueueHash(Properties connectionProp) {
		Hashtable queueHash = new Hashtable();
		queueHash.put(MQC.HOST_NAME_PROPERTY, connectionProp.getProperty(QueueConstants.QUEUE_HOSTNAME));
		queueHash.put(MQC.PORT_PROPERTY, Integer.parseInt(connectionProp.getProperty(QueueConstants.QUEUE_PORT)));
		queueHash.put(MQC.CHANNEL_PROPERTY, connectionProp.getProperty(QueueConstants.QUEUE_CHANNEL));
		return queueHash;
	}

}
