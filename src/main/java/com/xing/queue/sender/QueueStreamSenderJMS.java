package com.xing.queue.sender;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import com.xing.queue.BaseQueueStream;
import org.apache.log4j.Logger;

/**
 * Class to invoke MQ using JMS, the expected input is a BO name, which is
 * compatible with property file values. The class will connect to MQ and will
 * put the message to queue corresponding to the BO.
 */
public class QueueStreamSenderJMS extends BaseQueueStream implements QueueStreamSender {
	private static final Logger LOG = Logger.getLogger(QueueStreamSenderJMS.class);

	private volatile static QueueStreamSenderJMS queueStreamSenderJMS;

	public static QueueStreamSenderJMS getInstance() {
		if (queueStreamSenderJMS == null) {
			synchronized (QueueStreamSenderJMS.class) {
				if (queueStreamSenderJMS == null) {
					queueStreamSenderJMS = new QueueStreamSenderJMS();
				}
			}
		}
		return queueStreamSenderJMS;
	}

	public void invokeQueue(String boName, String message) {
		try {
			Connection queueConnection = createQueueConnection();
			queueConnection.start();

			Session queueSession = queueConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			Destination queue = getQueueFromBOName(boName, queueSession);
			
			MessageProducer producer = queueSession.createProducer(queue);
			TextMessage txtMessage = queueSession.createTextMessage(message);
			producer.send(txtMessage);
			LOG.info("Connected to Queue : Success");
			LOG.info(txtMessage);
			producer.close();
			queueSession.close();
			queueConnection.close();
		} catch (JMSException e) {	
			e.printStackTrace();
			LOG.error(("An IOException occured whilst writing to the message buffer: " + e.getStackTrace().toString()));
		} catch (Exception e) {		
			LOG.error(("An unknown exception occured whilst writing to the message buffer: "
					+ e.getStackTrace().toString()));

		}

	}

	
}
