package com.xing.queue.sender;

public class QueueStreamSenderFactory {		
	private static final String JMS = "JMS";
	private static final String JAVA = "JAVA";
	
	public static QueueStreamSenderFactory getInstance() {		
		return new QueueStreamSenderFactory();
	}

	public QueueStreamSender getQueueStreamSender(String mode) {
		if (mode == null) {
			return null;
		}
		if (mode.equalsIgnoreCase(JAVA)) {
			return QueueStreamSenderJava.getInstance();

		} else if (mode.equalsIgnoreCase(JMS)) {
			return QueueStreamSenderJMS.getInstance();

		}
		return null;

	}

}
