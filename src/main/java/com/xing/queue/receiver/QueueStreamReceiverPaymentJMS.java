package com.xing.queue.receiver;

import java.io.IOException;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;

import com.xing.queue.BaseQueueStream;
import org.apache.log4j.Logger;

import com.hdfc.core.core.util.CoreDataUtil;
import com.hdfc.core.core.util.GAEvent;
import com.hdfc.core.core.util.StreamConstants;
import com.xing.queue.sender.QueueStreamSenderJMS;

/**
 * Class to invoke MQ using JMS, the expected input is a BO name, which is
 * compatible with property file values. The class will connect to MQ and will
 * put the message to queue corresponding to the BO.
 */
public class QueueStreamReceiverPaymentJMS extends BaseQueueStream implements MessageListener, QueueStreamReceiver {
	private static final Logger LOG = Logger.getLogger(QueueStreamReceiverPaymentJMS.class);

	private volatile static QueueStreamReceiverPaymentJMS queueStreamReceiverPaymentJMS;

	public static QueueStreamReceiverPaymentJMS getInstance() {
		if (queueStreamReceiverPaymentJMS == null) {
			synchronized (QueueStreamSenderJMS.class) {
				if (queueStreamReceiverPaymentJMS == null) {
					queueStreamReceiverPaymentJMS = new QueueStreamReceiverPaymentJMS();
				}
			}
		}
		return queueStreamReceiverPaymentJMS;
	}

	public void onMessage(Message message) {
		try {
			CoreDataUtil.pushToGAStream(StreamConstants.EVENT_TYPE_RESP, GAEvent.PAYMENT_BO.getEventName(), message.getJMSMessageID());
		} catch (IOException e) {	
			LOG.error(("An IOException occured whilst writing to the message stream: " + e.getStackTrace().toString()));
		} catch (JMSException e) {			
			LOG.error(("An JMSException occured whilst writing to the message stream: " + e.getStackTrace().toString()));
		}

	}

	public void receiveQueue() {

		try {
			Connection queueConnection = createQueueConnection();

			Session queueSession = queueConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			Destination queue = getQueueFromBOName(GAEvent.PAYMENT_BO.toString(), queueSession);
			
			
			while (true) {
				MessageConsumer consumer = queueSession.createConsumer(queue);
				consumer.setMessageListener(new QueueStreamReceiverPaymentJMS());
				queueConnection.start();
				consumer.close();
			}

		} catch (JMSException e) {		
			e.printStackTrace();
			LOG.error(("An IOException occured whilst writing to the message buffer: " + e.getStackTrace().toString()));
		} catch (Exception e) {		
			LOG.error(("An unknown exception occured whilst writing to the message buffer: "
					+ e.getStackTrace().toString()));

		}
	}

}
