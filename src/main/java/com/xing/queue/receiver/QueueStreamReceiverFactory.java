package com.xing.queue.receiver;

import com.hdfc.core.core.util.GAEvent;

public class QueueStreamReceiverFactory {
	private static final String JMS = "JMS";
	private static final String JAVA = "JAVA";

	public static QueueStreamReceiverFactory getInstance() {
		return new QueueStreamReceiverFactory();
	}

	public QueueStreamReceiver getQueueStreamReceiver(String mode, String boName) {
		if (mode == null) {
			return null;
		}
		if (mode.equalsIgnoreCase(JAVA)) {
			return QueueStreamReceiverJava.getInstance();

		} else if (mode.equalsIgnoreCase(JMS)) {
			if (GAEvent.SUMBIT_BO.toString().equals(boName)) {
				return QueueStreamReceiverSubmitJMS.getInstance();
			} else if (GAEvent.PAYMENT_BO.toString().equals(boName)) {
				return QueueStreamReceiverPaymentJMS.getInstance();
			}

		}
		return null;

	}

}
