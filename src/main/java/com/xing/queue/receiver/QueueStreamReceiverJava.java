package com.xing.queue.receiver;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.log4j.Logger;

import com.xing.queue.sender.QueueStreamSenderJMS;

/**
 * Class to invoke MQ using Java, the expected input is a BO name, which is
 * compatible with property file values. The class will connect to MQ and will
 * put the message to queue corresponding to the BO.
 */
public class QueueStreamReceiverJava implements MessageListener,QueueStreamReceiver {
	private static final Logger LOG = Logger.getLogger(QueueStreamReceiverJava.class);
	private volatile static QueueStreamReceiverJava queueStreamReceiveJMS;

	public static QueueStreamReceiverJava getInstance() {
		if (queueStreamReceiveJMS == null) {
			synchronized (QueueStreamSenderJMS.class) {
				if (queueStreamReceiveJMS == null) {
					queueStreamReceiveJMS = new QueueStreamReceiverJava();
				}
			}
		}
		return queueStreamReceiveJMS;
	}

	public void receiveQueue() {
		// TODO Auto-generated method stub
		
	}

	public void onMessage(Message arg0) {
		// TODO Auto-generated method stub
		
	}

}
