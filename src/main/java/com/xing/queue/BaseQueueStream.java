package com.xing.queue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

import com.hdfc.core.core.util.CorePropertyUtil;
import com.xing.queue.util.QueueConstants;
import com.xing.queue.util.QueueUtil;
import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;

public class BaseQueueStream {
	Properties connectionProp;

	public BaseQueueStream() {
		super();
	}

	protected JmsConnectionFactory getConfiguredQueueConnection(Properties connectionProp) throws JMSException {
		JmsFactoryFactory jmsFactory = JmsFactoryFactory.getInstance(WMQConstants.WMQ_PROVIDER);
		JmsConnectionFactory jmsConnFactory = jmsFactory.createConnectionFactory();
		String hostName = connectionProp.getProperty(QueueConstants.QUEUE_HOSTNAME);
		String channel = connectionProp.getProperty(QueueConstants.QUEUE_CHANNEL);
		String queueManager = connectionProp.getProperty(QueueConstants.QUEUE_MANAGER_NAME);
		int port = Integer.parseInt(connectionProp.getProperty(QueueConstants.QUEUE_PORT));
		jmsConnFactory.setStringProperty(WMQConstants.WMQ_HOST_NAME, hostName);
		jmsConnFactory.setIntProperty(WMQConstants.WMQ_PORT, port);
		jmsConnFactory.setStringProperty(WMQConstants.WMQ_CHANNEL, channel);
		jmsConnFactory.setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		jmsConnFactory.setStringProperty(WMQConstants.WMQ_QUEUE_MANAGER, queueManager);
		return jmsConnFactory;
	}

	protected Connection createQueueConnection() throws FileNotFoundException, IOException, JMSException {
		connectionProp = CorePropertyUtil.loadClassPathProperties("connection.properties");
		JmsConnectionFactory mqQueueConnectionFactory = getConfiguredQueueConnection(connectionProp);
		String userName = connectionProp.getProperty(QueueConstants.QUEUE_USER_NAME);
		String password = connectionProp.getProperty(QueueConstants.QUEUE_PASSWORD);
		Connection queueConnection = mqQueueConnectionFactory.createConnection(userName, password);		
		return queueConnection;

	}

	protected Destination getQueueFromBOName(String boName, Session queueSession)
			throws JMSException, FileNotFoundException, IOException {
		String queueNameKey = QueueUtil.createQueueNameRequestKey(boName);
		if (connectionProp == null) {
			connectionProp = CorePropertyUtil.loadClassPathProperties("connection.properties");
		}
		Destination queue = queueSession.createQueue(connectionProp.getProperty(queueNameKey));
		return queue;
	}

}