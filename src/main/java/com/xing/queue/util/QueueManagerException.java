package com.xing.queue.util;

/**
 * Custom exception for the application
 *
 */

public class QueueManagerException extends Throwable {


	private static final long serialVersionUID = 1085850970625175580L;

	private Integer errorCode;

	private String errorMessage;

	private Throwable error;

	/**
	 * Constructor with code and message
	 * 
	 * @param errorCode
	 * @param errorMessage
	 */
	public QueueManagerException(Integer errorCode, String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	/**
	 * Constructor with code, message and exception object
	 * 
	 * @param errorCode
	 * @param errorMessage
	 * @param error
	 */
	public QueueManagerException(Integer errorCode, String errorMessage, Throwable error) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.error = error;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Throwable getError() {
		return error;
	}

	public void setError(Throwable error) {
		this.error = error;
	}




}





