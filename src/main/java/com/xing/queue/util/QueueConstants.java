package com.xing.queue.util;

public class QueueConstants {
	public static final String QUEUE_CHANNEL = "queue.channel";
	public static final String QUEUE_PORT = "queue.port";
	public static final String QUEUE_HOSTNAME = "queue.hostname";
	public static final String QUEUE_MANAGER_NAME = "queue.manager.name";
	public static final String QUEUE_USER_NAME = "queue.username";
	public static final String QUEUE_PASSWORD = "queue.password";
}
