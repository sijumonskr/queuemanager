package com.xing.queue;

import com.xing.queue.sender.QueueStreamSender;
import com.xing.queue.sender.QueueStreamSenderFactory;

public class QueueStreamSenderDemo {
	public static void main(String[] args) {
		// Code to invoke JMS implementation
		QueueStreamSenderFactory qFactory = QueueStreamSenderFactory.getInstance();
		QueueStreamSender senderJMS  = qFactory.getQueueStreamSender("JMS");
		senderJMS.invokeQueue("SUMBIT_BO","message from queue");
		
		// Code to invoke Java implementation
//		QueueStreamSender senderJava  = qFactory.getQueueStreamSender("JAVA");
//		senderJava.invokeQueue("quotation","Hello");
	}

}
