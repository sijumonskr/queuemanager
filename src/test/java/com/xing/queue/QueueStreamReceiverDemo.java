package com.xing.queue;

import com.xing.queue.receiver.QueueStreamReceiver;
import com.xing.queue.receiver.QueueStreamReceiverFactory;

public class QueueStreamReceiverDemo {
	public static void main(String[] args) {
		// Code to invoke JMS implementation
		QueueStreamReceiverFactory qFactory = QueueStreamReceiverFactory.getInstance();
		QueueStreamReceiver receiverJms = qFactory.getQueueStreamReceiver("JMS","SUMBIT_BO");
		receiverJms.receiveQueue(); 

		// Code to invoke Java implementation
		// QueueStreamSender senderJava = qFactory.getQueueStreamSender("JAVA");
		// senderJava.invokeQueue("quotation","Hello");
	}

}
